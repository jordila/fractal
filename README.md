# Fractal
> A Jekyll theme based on Minima, with support for coding icons and an infinite number of nested directory levels

[![GitHub tag](https://img.shields.io/github/tag/MichaelCurrin/fractal?include_prereleases&sort=semver)](https://github.com/MichaelCurrin/fractal/releases/)
[![License](https://img.shields.io/badge/License-MIT-blue)](/docs/license.md)

[![Jekyll](https://img.shields.io/badge/Jekyll-3.9-blue?logo=jekyll&logoColor=white)](https://jekyllrb.com)


<div align="center">

[![View demo - GH Pages](https://img.shields.io/badge/View_demo-GH_Pages-2ea44f?style=for-the-badge)](https://michaelcurrin.github.io/fractal/)

[![View - Documentation](https://img.shields.io/badge/View-Documentation-blue?style=for-the-badge)](/docs/)

</div>


## Quick reference
> Jump to code

- [logos](/_includes/logos/)
- [sections.html](/_includes/structure/sections.html)
- [page-list.html](/_includes/structure/page-list.html)
