# Flexbox

You can use the [page.html](/_layouts/page.html) layout which uses the flexbox pattern already.

Or use custom HTML as below, which will work because of the project's CSS added to the base Minima CSS styles.


## Code

{% raw %}
```html
<div class="flex-container">
    <a href="#">
        <div>
            {% include logo.html name="python" %}
            <span>Python</span>
        </div>
    </a>
    <a href="#">
        <div>
            {% include logo.html name="javascript" %}
            <span>JavaScript</span>
        </div>
    </a>
</div>
```
{% endraw %}


## Result

<div class="flex-container">
    <a href="#">
        <div>
            {% include logo.html name="python" %}
            <span>Python</span>
        </div>
    </a>
    <a href="#">
        <div>
            {% include logo.html name="javascript" %}
            <span>JavaScript</span>
        </div>
    </a>
</div>



### Sagan Ipsum

Cosmic ocean gathered by gravity sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem astonishment emerged into consciousness rings of Uranus? Finite but unbounded are creatures of the cosmos Apollonius of Perga kindling the energy hidden in matter a very small stage in a vast cosmic arena take root and flourish. The ash of stellar alchemy quis nostrum exercitationem ullam corporis suscipit laboriosam vel illum qui dolorem eum fugiat quo voluptas nulla pariatur Sea of Tranquility concept of the number one the only home we've ever known and billions upon billions upon billions upon billions upon billions upon billions upon billions.

Something incredible is waiting to be known rogue billions upon billions consciousness trillion another world. Laws of physics dream of the mind's eye emerged into consciousness Vangelis vanquish the impossible shores of the cosmic ocean? Two ghostly white figures in coveralls and helmets are softly dancing astonishment the only home we've ever known from which we spring extraordinary claims require extraordinary evidence vastness is bearable only through love and billions upon billions upon billions upon billions upon billions upon billions upon billions.

> Nihil Sine Kaos
